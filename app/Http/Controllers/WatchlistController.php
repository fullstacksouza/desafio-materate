<?php

namespace App\Http\Controllers;

use App\Watchlist;
use App\Movie;
use Illuminate\Http\Request;

class WatchlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getWatchlists(Request $request)
    {
        $watchlist = \Auth::user()->watchlists;
        return ['watchlists' => $watchlist];
    }
    public function index(Request $request)
    {
        $watchlists = Watchlist::find($request->watchlist);

        return response()->json(['watchlist_movies' => $watchlists->movies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return $request->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movie = Movie::find($request->movie);
        $watchlist = Watchlist::find($request->watchlist);
        $watchlist->movies()->save($movie);
        return response()->json(['watchlist' => $watchlist]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Watchlist  $watchlist
     * @return \Illuminate\Http\Response
     */
    public function show(Watchlist $watchlist)
    {
        //
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Watchlist  $watchlist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $watchlist = Watchlist::find($request->watchlist);
        $movie = Movie::find($request->movie);
        $watchlist->movies()->detach($movie);
        return $watchlist->withOut('movie_ids')->get();
        return response()->json(['watchlist' => $watchlist]);
    }
}
