<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Watchlist extends Eloquent
{
    protected $fillable = ['user_id'];
    public function user()
    {
        return $this->hasOne('App\User', 'user_id');
    }

    public function movies()
    {
        return $this->belongsToMany('App\Movie');
    }
}
