<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Movie extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'movies';
    protected $primaryKey = '_id';
    protected $fillable = [
        'title', 'description', 'year', 'cover'
    ];

    public function watchlists()
    {
        return $this->belongsToMany('App\Watchlist');
    }
}
