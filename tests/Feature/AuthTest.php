<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tymon\JWTAuth\JWTAuth;
class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */


    public function testLogin()
    {
        $credentials = [
            'email' => 'CHALLENGE@materate.com',
            'password' => 'mateRATEbackEND'
        ];
        $response = $this->json('POST', route('auth'), $credentials);
        $response->assertStatus(200);
        $this->assertArrayHasKey('token', $response->json());
    }


}
