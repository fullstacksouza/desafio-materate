<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
class WatchlistTest extends TestCase
{
    protected $watchlist = '5cccae7927b3290ae20fdc8e';
    public function getToken()
    {
        $user = User::where('CHALLENGE@materate.com')->first();
        $token =  auth('api')->login($user);
        return $token;
    }
    public function testAddMovieToWatchList()
    {

        $token = $this->getToken();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', route('watchlists.movies.store', [
            'watchlist' => $this->watchlist,
            'movie' => '5cccae7927b3290ae20fdc83'
        ]));
        $response->assertStatus(200);
    }

    public function testRemoveMovieFromWatchlist()
    {
        $token = $this->getToken();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('delete', route('watchlists.movies.destroy', [
            'watchlist' => $this->watchlist,
            'movie' => '5cccae7927b3290ae20fdc83'
        ]));
        $response->assertStatus(200);
    }
    public function testMoviesInWatchlist()
    {
        $token = $this->getToken();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('get', route('watchlists.movies.index', ['watchlist' => $this->watchlist]));
        $response->assertStatus(200);
    }
}
