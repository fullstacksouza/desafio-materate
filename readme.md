## Instalação

Após fazer download do repositório, entrar na pasta em que foi baixado e executar os comandos:

`$ cp .env.example .env`
<br>

Instalar dependencias com o composer:

`$ composer update`

Configurar acesso ao banco de dados:

No arquivo .env alterar as seguintes variavéis

    MONGO_DB_HOST=host do banco de dados
    MONGO_DB_PORT= porta
    MONGO_DB_DATABASE= nome do banco
    MONGO_DB_USERNAME= usuario
    MONGO_DB_PASSWORD= senha

Após configurar o banco de dados rodar cadastrar os filmes

`php artisan db:seed`

iniciando o servidor

`php artisan serve`

Para rodar os testes:

`vendor/bin/phpunit`

## Collection do postman com as rotas:

https://www.getpostman.com/collections/4534a92f1bbbdb854b6f

## Testando rotas no postman

Primeiramente temos que configurar as variaves de ambiente do postman, as variaveis a serem utilizadas são:

base_url = url da aplicação

token = token da autenticação

watchlist = id da watchlist

## Recebendo o token;

Acessar a request com o nome Autenticação passando os parametros email e password

a resposta da requisição,caso as credenciais passadas forem corretas será:

    {
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjEuMjY6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTU1Njk3NjY4MiwiZXhwIjoxNTU3MDE3NDgyLCJuYmYiOjE1NTY5NzY2ODIsImp0aSI6IjhMZkl5Zml5bnBCQUw0V2ciLCJzdWIiOiI1Y2NjYWU3ODI3YjMyOTBhZTIwZmRjODIiLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.p52Zbpwlhf1o9Vq3rE89Dj9WuWHcxPWdIyQNCKHuDq0",
    "expires": 40800
    }

Após autenticar, pegaremos esse token recebido, e vamos setar na variavel de ambiente "token" no postman para prosseguir com as requisiçoes

Agora vamos buscar o id da watchlist que ja está pré cadastrada

Acessar a request "Watchlists cadastradas"

Obs: todoas as rotas a partir de agora necessitam de uma Autorização do header

tipo da autorização:Bearer Token cujo o token é o que recebemos anteriormente na request de autenticação

A reposta da requisição será

    {
    "watchlists": [
        {
            "_id": "5cccae7927b3290ae20fdc8e",
            "name": "Minha Lista de Filmes",
            "user_id": "5cccae7827b3290ae20fdc82",
            "updated_at": "2019-05-03 21:11:21",
            "created_at": "2019-05-03 21:11:21",
            "movie_ids": []
        }
    ]
    }

Pegamos o id dessa watchlist e setamos na variavel de ambiente do postman "watchlist"

## Listar filmes cadastrados na watchlists

Acessar request "Filmes cadastrados na watchlist"

exemplo de resposta:

```json
{
    "watchlist_movies": [
        {
            "_id": "5cccae7927b3290ae20fdc8d",
            "title": "Vingadores: Ultimato",
            "release_year": "2019",
            "genres": "Ação, Aventura, Ficção científica",
            "updated_at": "2019-05-03 21:11:21",
            "created_at": "2019-05-03 21:11:21",
            "watchlist_ids": ["5cccae7927b3290ae20fdc8e"]
        }
    ]
}
```

## Adicionar filmes na watchlists

Acessar request "Adicionar filme na watchlist"

exemplo de resposta:

```json
{
    "watchlist": {
        "_id": "5cccae7927b3290ae20fdc8e",
        "name": "Minha Lista de Filmes",
        "user_id": "5cccae7827b3290ae20fdc82",
        "updated_at": "2019-05-03 21:11:21",
        "created_at": "2019-05-03 21:11:21",
        "movie_ids": ["5cccae7927b3290ae20fdc8d"]
    }
}
```

## Remover filmes na watchlists

Acessar request "Remover filme na watchlist"

exemplo de resposta:

```json
[
    {
        "_id": "5cccae7927b3290ae20fdc8e",
        "name": "Minha Lista de Filmes",
        "user_id": "5cccae7827b3290ae20fdc82",
        "updated_at": "2019-05-03 21:11:21",
        "created_at": "2019-05-03 21:11:21",
        "movie_ids": []
    }
]
```

# Testando rotas via Curl

## Autenticação

```
curl -X POST \
  'http://localhost:8000/api/login?email=CHALLENGE@materate.com&password=mateRATEbackEND' \
  -H 'Postman-Token: 1fa21f82-2385-4fdb-8ed3-73904b0f09e5' \
  -H 'cache-control: no-cache'
```

## Adicionar filme na watchlist

```
curl -X POST \
  'http://localhost:8000/api/watchlists/{watchlist}/movies?movie={movie}' \
  -H 'Authorization: Bearer {token}' \
  -H 'Postman-Token: c7d68ba9-2fb2-4891-b45a-296d456bb1c4' \
  -H 'cache-control: no-cache'
```

## Listar filmes na watchlist

```
curl -X GET \
  http://192.168.1.26:8000/api/watchlists/{watchlist}/movies \
  -H 'Authorization: Bearer {token}' \
  -H 'Postman-Token: 27a08453-4dbc-4578-9dbf-b3b8dd179340' \
  -H 'cache-control: no-cache'

```

## Remover filme na watchlist

```
curl -X DELETE \
  http://localhost:8000/api/watchlists/{watchlist/movies/{movie} \
  -H 'Authorization: Bearer {token}' \
  -H 'cache-control: no-cache'
```
