<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Materate',
            'email' => 'CHALLENGE@materate.com',
            'password' => bcrypt('mateRATEbackEND')
        ]);
    }
}
