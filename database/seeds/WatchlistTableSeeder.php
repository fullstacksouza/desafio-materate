<?php

use Illuminate\Database\Seeder;
use App\Watchlist;
use App\User;

class WatchlistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'CHALLENGE@materate.com')->first();
        $watchlist = $user->watchlists()->create([
            'name' => 'Minha Lista de Filmes',
            'user_id' => $user->_id
        ]);
    }
}
