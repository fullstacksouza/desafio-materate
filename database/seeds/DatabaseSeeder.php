<?php

use Illuminate\Database\Seeder;
// use MoviesTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(UsersTableSeed::class);
        $this->call(MoviesTableSeeder::class);
        $this->call(WatchlistTableSeeder::class);
    }
}
