<?php

use Illuminate\Database\Seeder;
use App\Movie;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movies = [];
        $movies[] = ['title' => 'Piratas Do Caribe: A Maldição Do Pérola Negra', 'release_year' => "2003", 'genres' => "Aventura, Fantasia, Ação"];
        $movies[] = ['title' => 'Aventura, Fantasia, Ação', 'release_year' => "2014", 'genres' => "Ação, Aventura, Fantasia, Ficção científica"];
        $movies[] = ['title' => 'Como Treinar o Seu Dragão 3', 'release_year' => "2019", 'genres' => "Animação, Família, Aventura"];
        $movies[] = ['title' => 'Velozes e Furiosos 7', 'release_year' => "2015", 'genres' => " Ação, Crime, Suspense"];
        $movies[] = ['title' => 'Kingsman: Serviço Secreto', 'release_year' => "2015", 'genres' => "Crime, Comédia, Ação, Aventura"];
        $movies[] = ['title' => 'Hannah Gadsby: Nanette', 'release_year' => "2018", 'genres' => "Comédia"];
        $movies[] = ['title' => 'Um Sonho de Liberdade', 'release_year' => "1995", 'genres' => "Um Sonho de Liberdade"];
        $movies[] = ['title' => 'Duelo De Titãs', 'release_year' => "2001", 'genres' => "Drama"];
        $movies[] = ['title' => 'No Auge da Fama', 'release_year' => "2014", 'genres' => "Drama, Comédia"];
        $movies[] = ['title' => 'Venom', 'release_year' => "2018", 'genres' => "Ficção científica, Ação"];
        $movies[] = ['title' => 'Vingadores: Ultimato', 'release_year' => "2019", 'genres' => "Ação, Aventura, Ficção científica"];
        foreach ($movies as $movie) {
            Movie::create($movie);
        }
    }
}
